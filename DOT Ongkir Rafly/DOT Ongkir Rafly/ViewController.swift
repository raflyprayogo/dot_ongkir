//
//  ViewController.swift
//  DOT Ongkir Rafly
//
//  Created by Rafly Prayogo on 03/07/18.
//  Copyright © 2018 dot-indonesia. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import CoreData

class ViewController: UIViewController {
    
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    var context:NSManagedObjectContext?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        context = appDelegate.persistentContainer.viewContext
        
//        fetchProv()
        fetchCity()
//        getDataCity()
//        getDataProvince()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func getDataProvince(){
        let entity      = NSEntityDescription.entity(forEntityName: "Province", in: context!)
        let headers     = ["key": AppConfig().APIKEY_RO]
        Alamofire.request(AppConfig().URL_RO_PROVINCE, headers: headers).responseJSON { response in
            switch response.result {
            case .success:
                let json    = JSON(response.result.value!)
                print(json)
                let results = json["rajaongkir"]["results"].arrayObject
                
                for resItem in results as! [Dictionary<String, AnyObject>] {
                    let provinceID     = resItem["province_id"] as? String ?? ""
                    let provinceName   = resItem["province"] as? String ?? ""
                    
                    let newData     = NSManagedObject(entity: entity!, insertInto: self.context!)
                    newData.setValue(Int32(provinceID), forKey: "province_id")
                    newData.setValue(provinceName, forKey: "province_name")
                    do {
                        try self.context!.save()
                    } catch {
                        print("problem saving")
                    }
                }
            case .failure(let error):
                print(error)
            }
        }
    }
    
    func getDataCity(){
        let entity      = NSEntityDescription.entity(forEntityName: "City", in: context!)
        let headers     = ["key": AppConfig().APIKEY_RO]
        Alamofire.request(AppConfig().URL_RO_CITY, headers: headers).responseJSON { response in
            switch response.result {
            case .success:
                let json    = JSON(response.result.value!)
                print(json)
                let results = json["rajaongkir"]["results"].arrayObject
                
                for resItem in results as! [Dictionary<String, AnyObject>] {
                    let cityID      = resItem["city_id"] as? String ?? ""
                    let provinceID  = resItem["province_id"] as? String ?? ""
                    let type        = resItem["type"] as? String ?? ""
                    let cityPost    = resItem["postal_code"] as? String ?? ""
                    let cityName    = resItem["city_name"] as? String ?? ""
                    
                    let newData     = NSManagedObject(entity: entity!, insertInto: self.context!)
                    newData.setValue(Int32(cityID), forKey: "city_id")
                    newData.setValue(Int32(provinceID), forKey: "city_province_id")
                    newData.setValue(Int32(cityPost), forKey: "city_postal_code")
                    newData.setValue(type, forKey: "city_type")
                    newData.setValue(cityName, forKey: "city_name")
                    do {
                        try self.context!.save()
                    } catch {
                        print("problem saving")
                    }
                }
            case .failure(let error):
                print(error)
            }
        }
    }
    
    func fetchProv(){
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "Province")
        request.returnsObjectsAsFaults = false
        
//        if let result = try? context!.fetch(request) {
//            for object in result {
//                context!.delete(object as! NSManagedObject)
//            }
//        }
//
//        do {
//            try context!.save() // <- remember to put this :)
//        } catch {
//            // Do something... fatalerror
//        }
        
        do {
            let result = try self.context!.fetch(request)
            for data in result as! [NSManagedObject] {
                print( data.value(forKey: "province_name") as! String)
                print( data.value(forKey: "province_id") as! Int32)
            }
            
        } catch {
            
            print("Failed")
        }
    }
    
    func fetchCity(){
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "City")
        request.returnsObjectsAsFaults = false
        
                if let result = try? context!.fetch(request) {
                    for object in result {
                        context!.delete(object as! NSManagedObject)
                    }
                }
        
                do {
                    try context!.save() // <- remember to put this :)
                } catch {
                    // Do something... fatalerror
                }
        
//        do {
//            let result = try self.context!.fetch(request)
//            for data in result as! [NSManagedObject] {
//                print( data.value(forKey: "city_name") as! String)
//                print( data.value(forKey: "city_id") as! Int32)
//                print( data.value(forKey: "city_province_id") as! Int32)
//                print( data.value(forKey: "city_postal_code") as! Int32)
//                print( data.value(forKey: "city_type") as! String)
//            }
//
//        } catch {
//
//            print("Failed")
//        }
    }

}

