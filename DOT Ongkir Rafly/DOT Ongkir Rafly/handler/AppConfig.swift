//
//  AppConfig.swift
//  DOT Ongkir Rafly
//
//  Created by Rafly Prayogo on 03/07/18.
//  Copyright © 2018 dot-indonesia. All rights reserved.
//

import Foundation
import UIKit

class AppConfig: NSObject {
    
    //=============== RAJA ONGKIR ===============
    let APIKEY_RO                   = "0df6d5bf733214af6c6644eb8717c92c"
    let URL_RO_PROVINCE             = "https://api.rajaongkir.com/starter/province"
    let URL_RO_CITY                 = "https://api.rajaongkir.com/starter/city"
    let URL_RO_COST                 = "https://api.rajaongkir.com/starter/cost"
    
    //main color #A1887F
    //sec color #3E2723
    
}
