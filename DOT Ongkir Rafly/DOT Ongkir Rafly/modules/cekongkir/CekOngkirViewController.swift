//
//  CekOngkirViewController.swift
//  DOT Ongkir Rafly
//
//  Created by Rafly Prayogo on 04/07/18.
//  Copyright © 2018 dot-indonesia. All rights reserved.
//

import UIKit
import DropDown
import SearchTextField
import CoreData
import Alamofire
import SwiftyJSON

class CekOngkirViewController: UIViewController, UIPickerViewDelegate, UIPickerViewDataSource {
    
    @IBOutlet weak var courierPicker: UIPickerView!
    @IBOutlet weak var weightTf: UITextField!
    @IBOutlet weak var destinationTf: SearchTextField!
    @IBOutlet weak var originTf: SearchTextField!
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    var context:NSManagedObjectContext?
    var progressView:ProgressView?
    let session = UserDefaults.standard
    
    var cityIDs     = Dictionary<String, String>()
    var cityItems   = [SearchTextFieldItem]()
    let courier     = ["JNE","TIKI","Pos Indonesia"]
    
    var selOriginID:String = ""
    var selDestID:String   = ""
    var selCourier:String  = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title  = "Cek Ongkir"
        context     = appDelegate.persistentContainer.viewContext
        selCourier  = "jne"
        
        courierPicker.delegate          = self
        courierPicker.dataSource        = self
        
        originTf.maxNumberOfResults         = 5
        destinationTf.maxNumberOfResults    = 5
        
        originTf.itemSelectionHandler = { filteredResults, itemPosition in
            let item            = filteredResults[itemPosition]
            self.selOriginID    = self.cityIDs[item.title]!
            
            self.originTf.text  = item.title
        }
        
        destinationTf.itemSelectionHandler = { filteredResults, itemPosition in
            let item        = filteredResults[itemPosition]
            self.selDestID  = self.cityIDs[item.title]!
            
            self.destinationTf.text  = item.title
        }
        
        getDataCity()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        self.progressView = ProgressView()
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
        textField.resignFirstResponder()
        return true
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return courier.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return courier[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent  component: Int) {
        if(row == 0){
            selCourier = "jne"
        }else if(row == 1){
            selCourier = "tiki"
        }else{
            selCourier = "pos"
        }
    }
    
    func getDataCity(){
        cityItems.removeAll()
        cityIDs.removeAll()
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "City")
        request.returnsObjectsAsFaults = false
        
        do {
            let result = try self.context!.fetch(request)
            
            if(result.count == 0){
                self.getDataAPI()
            }
            
            for data in result as! [NSManagedObject] {
                let city    = data.value(forKey: "city_name") as! String
                let cityId  = String(data.value(forKey: "city_id") as! Int32)
                let prov    = data.value(forKey: "city_province_name") as! String
                var type    = data.value(forKey: "city_type") as! String
                if(type == "Kabupaten"){
                    type = " (KAB)"
                }else{
                    type = " (KOT)"
                }
                
                let originItem = SearchTextFieldItem(title: city + type, subtitle: prov)
                self.cityItems.append(originItem)
                self.cityIDs[city + type] = cityId
            }
            DispatchQueue.main.async {
                self.originTf.filterItems(self.cityItems)
                self.destinationTf.filterItems(self.cityItems)
            }
        } catch {
            print("Failed")
        }
    }
    
    @IBAction func btnSearch(_ sender: Any) {
        if(!ViewUtil().validateTFEmpty([self.originTf, self.destinationTf, weightTf])){
            return
        }
        if(!ViewUtil().validateTFNumeric([self.weightTf])){
            return
        }
        
        if(!ViewUtil().validateStringEmpty(selOriginID, "Kota Asal")){
            return
        }
        if(!ViewUtil().validateStringEmpty(selDestID, "Kota Tujuan")){
            return
        }
        
        if(self.originTf.text?.trimmingCharacters(in: .whitespacesAndNewlines) == self.destinationTf.text?.trimmingCharacters(in: .whitespacesAndNewlines)){
            ViewUtil().failedMsg(msg: "Kota asal dan kota tujuan tidak boleh sama")
            return
        }
        
        self.progressView?.show()
        let headers     = ["key": AppConfig().APIKEY_RO]
        let parameters: Parameters = [
            "origin": selOriginID,
            "destination": selDestID,
            "weight": weightTf.text!.trimmingCharacters(in: .whitespacesAndNewlines),
            "courier": selCourier
        ]
        
        Alamofire.request(AppConfig().URL_RO_COST, method: .post, parameters: parameters, headers: headers).responseJSON { response in
            switch response.result {
            case .success:
                self.progressView?.hide()
                let json    = JSON(response.result.value!)
                let results = json["rajaongkir"]
                
                let destVC : DetailOngkirViewController = self.storyboard!.instantiateViewController(withIdentifier: "DetailOngkirVC") as! DetailOngkirViewController
                destVC.title                    = "Detail Layanan"
                destVC.hidesBottomBarWhenPushed = true
                destVC.dataJSON                 = results
                self.navigationController!.pushViewController(destVC, animated: true)
            case .failure(let error):
                print(error)
                self.progressView?.hide()
                ViewUtil().checkConn()
            }
        }
    }
    
    @IBAction func btnSync(_ sender: Any) {
        let alert = UIAlertController(title: "Perbarui data kota dan provinsi ?", message: "", preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "Perbarui", style: UIAlertActionStyle.default, handler: { action in
            self.getDataAPI()
        }))
        alert.addAction(UIAlertAction(title: "Batal", style: UIAlertActionStyle.cancel, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    func getDataAPI(){
        getDataProvinceAPI()
        getDataCityAPI()
    }
    
    func getDataProvinceAPI(){
        self.progressView?.show()
        let entity      = NSEntityDescription.entity(forEntityName: "Province", in: context!)
        
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "Province")
        request.returnsObjectsAsFaults = false
        if let result = try? context!.fetch(request) {
            for object in result {
                context!.delete(object as! NSManagedObject)
            }
        }
        do {
            try context!.save()
        } catch {
            print("error")
        }
        
        DispatchQueue.main.async {
            let headers     = ["key": AppConfig().APIKEY_RO]
            Alamofire.request(AppConfig().URL_RO_PROVINCE, headers: headers).responseJSON { response in
                switch response.result {
                case .success:
                    self.progressView?.hide()
                    let json    = JSON(response.result.value!)
                    print(json)
                    let results = json["rajaongkir"]["results"].arrayObject
                    
                    for resItem in results as! [Dictionary<String, AnyObject>] {
                        let provinceID     = resItem["province_id"] as? String ?? ""
                        let provinceName   = resItem["province"] as? String ?? ""
                        
                        let newData     = NSManagedObject(entity: entity!, insertInto: self.context!)
                        newData.setValue(Int32(provinceID), forKey: "province_id")
                        newData.setValue(provinceName, forKey: "province_name")
                        do {
                            try self.context!.save()
                        } catch {
                            ViewUtil().failedMsg(msg: "Gagal menyimpan data")
                        }
                    }
                case .failure(let error):
                    self.progressView?.hide()
                    print(error)
                    ViewUtil().checkConn()
                }
            }
        }
    }
    
    func getDataCityAPI(){
        self.progressView?.show()
        let entity      = NSEntityDescription.entity(forEntityName: "City", in: context!)
        
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "City")
        request.returnsObjectsAsFaults = false
        if let result = try? context!.fetch(request) {
            for object in result {
                context!.delete(object as! NSManagedObject)
            }
        }
        do {
            try context!.save()
        } catch {
            print("error")
        }
        
        DispatchQueue.main.async {
            let headers     = ["key": AppConfig().APIKEY_RO]
            Alamofire.request(AppConfig().URL_RO_CITY, headers: headers).responseJSON { response in
                switch response.result {
                case .success:
                    self.progressView?.hide()
                    let json    = JSON(response.result.value!)
                    print(json)
                    let results = json["rajaongkir"]["results"].arrayObject
                    
                    for resItem in results as! [Dictionary<String, AnyObject>] {
                        let cityID      = resItem["city_id"] as? String ?? ""
                        let provinceID  = resItem["province_id"] as? String ?? ""
                        let provinceName = resItem["province"] as? String ?? ""
                        let type        = resItem["type"] as? String ?? ""
                        let cityPost    = resItem["postal_code"] as? String ?? ""
                        let cityName    = resItem["city_name"] as? String ?? ""
                        
                        let newData     = NSManagedObject(entity: entity!, insertInto: self.context!)
                        newData.setValue(Int32(cityID), forKey: "city_id")
                        newData.setValue(Int32(provinceID), forKey: "city_province_id")
                        newData.setValue(Int32(cityPost), forKey: "city_postal_code")
                        newData.setValue(type, forKey: "city_type")
                        newData.setValue(cityName, forKey: "city_name")
                        newData.setValue(provinceName, forKey: "city_province_name")
                        do {
                            try self.context!.save()
                        } catch {
                            ViewUtil().failedMsg(msg: "Gagal menyimpan data")
                        }
                    }
                    
                    self.getDataCity()
                case .failure(let error):
                    self.progressView?.hide()
                    print(error)
                    ViewUtil().checkConn()
                }
            }
        }
    }
}
