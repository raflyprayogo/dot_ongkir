//
//  DetailOngkirViewController.swift
//  DOT Ongkir Rafly
//
//  Created by Rafly Prayogo on 04/07/18.
//  Copyright © 2018 dot-indonesia. All rights reserved.
//

import UIKit
import SwiftyJSON

class DetailOngkirTableViewCell: UITableViewCell{
    @IBOutlet weak var serviceLbl: UILabel!
    @IBOutlet weak var descLbl: UILabel!
    @IBOutlet weak var priceLbl: UILabel!
    @IBOutlet weak var etdLbl: UILabel!
    
}

class DetailOngkirViewController: UIViewController, UITableViewDataSource, UITableViewDelegate  {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var courierLbl: UILabel!
    @IBOutlet weak var originLbl: UILabel!
    @IBOutlet weak var destinationLbl: UILabel!
    @IBOutlet weak var postalOriginLbl: UILabel!
    @IBOutlet weak var postalDestLbl: UILabel!
    @IBOutlet weak var weightLbl: UILabel!
    @IBOutlet weak var courierLogoIv: UIImageView!
    
    var tblvContent   = [[String]]()
    var dataJSON:JSON?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        print(dataJSON)
        
        tableView.delegate              = self
        tableView.dataSource            = self
        tableView.estimatedRowHeight    = 63
        tableView.rowHeight             = UITableViewAutomaticDimension
        tableView.alwaysBounceVertical  = true
        
        let courierCode         = dataJSON!["query"]["courier"].description
        if(courierCode == "jne"){
            courierLogoIv.image = UIImage(named:"jne")
        }else if(courierCode == "tiki"){
            courierLogoIv.image = UIImage(named:"tiki")
        }else{
            courierLogoIv.image = UIImage(named:"pos")
        }
        
//        print(dataJSON!["results"][0]["name"])
        courierLbl.text         = dataJSON!["results"][0]["name"].description
        originLbl.text          = dataJSON!["origin_details"]["city_name"].description
        destinationLbl.text     = dataJSON!["destination_details"]["city_name"].description
        postalOriginLbl.text    = dataJSON!["origin_details"]["postal_code"].description
        postalDestLbl.text      = dataJSON!["destination_details"]["postal_code"].description
        weightLbl.text          = dataJSON!["query"]["weight"].description + " gram"
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        fetchDataService()
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return tblvContent.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "serviceCell", for: indexPath) as! DetailOngkirTableViewCell
        
        cell.serviceLbl.text        = tblvContent[indexPath.row][0]
        cell.descLbl.text           = tblvContent[indexPath.row][1]
        cell.priceLbl.text          = "Rp. " + tblvContent[indexPath.row][3]
        cell.etdLbl.text            = tblvContent[indexPath.row][2].replacingOccurrences(of: "HARI", with: "") + " Hari"
        
        return cell
    }
    
    @objc func fetchDataService(){
        let results = dataJSON!["results"][0]["costs"].arrayObject!
        for resItem in results as! [Dictionary<String, AnyObject>] {
            let name    = resItem["service"] as? String ?? ""
            let desc    = resItem["description"] as? String ?? ""
            if let cost     = resItem["cost"] as? [Dictionary<String, AnyObject>]{
                let etd     = cost[0]["etd"] as? String ?? ""
                let price   = cost[0]["value"] as? Int ?? 0
                
                self.tblvContent.append([name,desc,etd, price.stringWithSepator])
            }
        }
        DispatchQueue.main.async {
            self.tableView.reloadData()
        }
    }

}
