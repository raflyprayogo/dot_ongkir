//
//  GetCityViewController.swift
//  DOT Ongkir Rafly
//
//  Created by Rafly Prayogo on 04/07/18.
//  Copyright © 2018 dot-indonesia. All rights reserved.
//

import UIKit
import CoreData
import Alamofire
import SwiftyJSON

class CityTableViewCell: UITableViewCell{
    @IBOutlet weak var nameLbl: UILabel!
    @IBOutlet weak var provLbl: UILabel!
    @IBOutlet weak var postalLbl: UILabel!
    
}

class GetCityViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {

    @IBOutlet weak var tableView: UITableView!
    var tblvContent         = [[String]]()
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    var context:NSManagedObjectContext?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Kota"
        context = appDelegate.persistentContainer.viewContext
        
        tableView.delegate              = self
        tableView.dataSource            = self
        tableView.estimatedRowHeight    = 80
        tableView.rowHeight             = UITableViewAutomaticDimension
        tableView.alwaysBounceVertical  = true
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        getData()
//        getDataCity()
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return tblvContent.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cityCell", for: indexPath) as! CityTableViewCell
        
        var type = tblvContent[indexPath.row][4]
        if(type == "Kabupaten"){
            type = " (KAB)"
        }else{
            type = " (KOT)"
        }
        
        cell.nameLbl.text        = tblvContent[indexPath.row][3] + type
        cell.provLbl.text        = tblvContent[indexPath.row][1]
        cell.postalLbl.text      = tblvContent[indexPath.row][2]
        
        return cell
    }
    
    @objc func getData(){
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "City")
        request.returnsObjectsAsFaults = false
        
        do {
            let result = try self.context!.fetch(request)
            for data in result as! [NSManagedObject] {
                self.tblvContent.append([
                    String(data.value(forKey: "city_id") as! Int32),
                    data.value(forKey: "city_province_name") as! String,
                    String(data.value(forKey: "city_postal_code") as! Int32),
                    data.value(forKey: "city_name") as! String,
                    data.value(forKey: "city_type") as! String
                ])
            }
            DispatchQueue.main.async {
                self.tableView.reloadData()
            }
        } catch {
            print("Failed")
        }
    }
    
    func getDataCity(){
        tblvContent.removeAll()
        let entity      = NSEntityDescription.entity(forEntityName: "City", in: context!)
        let headers     = ["key": AppConfig().APIKEY_RO]
        Alamofire.request(AppConfig().URL_RO_CITY, headers: headers).responseJSON { response in
            switch response.result {
            case .success:
                let json    = JSON(response.result.value!)
                print(json)
                let results = json["rajaongkir"]["results"].arrayObject
                
                for resItem in results as! [Dictionary<String, AnyObject>] {
                    let cityID      = resItem["city_id"] as? String ?? ""
                    let provinceID  = resItem["province_id"] as? String ?? ""
                    let provinceName  = resItem["province"] as? String ?? ""
                    let type        = resItem["type"] as? String ?? ""
                    let cityPost    = resItem["postal_code"] as? String ?? ""
                    let cityName    = resItem["city_name"] as? String ?? ""
                    
                    let newData     = NSManagedObject(entity: entity!, insertInto: self.context!)
                    newData.setValue(Int32(cityID), forKey: "city_id")
                    newData.setValue(Int32(provinceID), forKey: "city_province_id")
                    newData.setValue(Int32(cityPost), forKey: "city_postal_code")
                    newData.setValue(type, forKey: "city_type")
                    newData.setValue(cityName, forKey: "city_name")
                    newData.setValue(provinceName, forKey: "city_province_name")
                    do {
                        try self.context!.save()
                    } catch {
                        print("problem saving")
                    }
                }
            case .failure(let error):
                print(error)
            }
        }
    }

}
