//
//  GetProvinceViewController.swift
//  DOT Ongkir Rafly
//
//  Created by Rafly Prayogo on 03/07/18.
//  Copyright © 2018 dot-indonesia. All rights reserved.
//

import UIKit
import CoreData

class ProvinceTableViewCell: UITableViewCell{
    @IBOutlet weak var nameLbl: UILabel!
    
}

class GetProvinceViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    @IBOutlet weak var tableView: UITableView!
    
    var tblvContent         = [[String]]()
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    var context:NSManagedObjectContext?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        context = appDelegate.persistentContainer.viewContext
        self.title = "Provinsi"

        tableView.delegate              = self
        tableView.dataSource            = self
        tableView.estimatedRowHeight    = 63
        tableView.rowHeight             = UITableViewAutomaticDimension
        tableView.alwaysBounceVertical  = true
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        getData()
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return tblvContent.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "provinceCell", for: indexPath) as! ProvinceTableViewCell
        
        cell.nameLbl.text        = tblvContent[indexPath.row][1]
        
        return cell
    }
    
    @objc func getData(){
        tblvContent.removeAll()
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "Province")
        request.returnsObjectsAsFaults = false
        
        do {
            let result = try self.context!.fetch(request)
            for data in result as! [NSManagedObject] {
                self.tblvContent.append([
                    String(data.value(forKey: "province_id") as! Int32),
                    data.value(forKey: "province_name") as! String
                ])
            }
            DispatchQueue.main.async {
                self.tableView.reloadData()
            }
        } catch {
            print("Failed")
        }
    }

}
